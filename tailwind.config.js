module.exports = {
  content: [
    "./src/**/*.{html,js}",
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  theme: {
    colors: {
      primary: '#fdd521',
      secondary: '#2185e9',
      tertiary: '#d94d44',
      quaternary: '#454545'
    },
    extend: {},
  },
  plugins: [],
}