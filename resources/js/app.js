import { createApp, h } from 'vue'
import { createInertiaApp, Link, Head } from '@inertiajs/inertia-vue3'
import Layout from "./Shared/Layout";

createInertiaApp({
  resolve: async name => {
    let page = (await import(`./Pages/${name}`)).default;
    
    // if the page doesn't have a Layout, use the Shared Layout file
    if(page.layout === undefined) {
      page.layout = Layout;
    }
    
    return page;
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin) // use Inertia
      .component('Link', Link)
      .component('Head', Head)
      .mount(el)
  },
  title: title => `My Game - ${title}`
});