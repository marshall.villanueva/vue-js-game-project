<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use \App\Http\Controllers\Auth\LoginController;
use \App\Http\Controllers\SessionsController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

ROUTE::get('/logout', [SessionsController::class, 'destroy']);

Route::get('login', [LoginController::class, 'create'])->name('login');
Route::post('login', [LoginController::class, 'store']);

Route::middleware('auth')->group(function () {

    Route::get('/', function () {
        return Inertia::render('Home');
    });

    Route::get('/rewards', function () {
        $user = Auth::user();
        $day_counter = $user->day_counter;
        $day_counter = json_decode($day_counter);
        $year = $day_counter->y;
        $month = $day_counter->m;
        $day = $day_counter->d;
        $hour = $day_counter->h;
        $minute = $day_counter->i;
        $seconds = $day_counter->s;
        $habit = $user->habit;
        return Inertia::render('Rewards', [
            'user' => $user,
            'name' =>$user->name,
            'last_time' => $user->last_time,
            'day_counter' => $user->day_counter,
            'year' => $year,
            'month' => $month,
            'day' => $day,
            'hour' => $hour,
            'minute' => $minute,
            'seconds' => $seconds,
            'habit' => $habit
        ]);
    });

    Route::get('/profile', function () {
        $user = Auth::user();
        $day_counter = $user->day_counter;
        if($day_counter){
            $day_counter = $user->day_counter;
            $day_counter = json_decode($day_counter);
            $year = $day_counter->y;
            $month = $day_counter->m;
            $day = $day_counter->d;
            $hour = $day_counter->h;
            $minute = $day_counter->i;
            $seconds = $day_counter->s;
        }
        else {
            $day_counter = null;
            $year = null;
            $month = null;
            $day = null;
            $hour = null;
            $minute = null;
            $seconds = null;
        }
        
        $habit = $user->habit;
        return Inertia::render('Profile', [
            'user' => $user,
            'name' =>$user->name,
            'last_time' => $user->last_time,
            'day_counter' => $user->day_counter,
            'year' => $year,
            'month' => $month,
            'day' => $day,
            'hour' => $hour,
            'minute' => $minute,
            'seconds' => $seconds,
            'habit' => $habit
        ]);
    });

    Route::post('/profile', function () {
        $user = Auth::user();
        $today = new DateTime(date('Y-m-d H:i:s'));
        $user->today = $today;
        $last_time = $user->last_time;
        $last_time = new DateTime($last_time);
        $user->day_counter = json_encode(date_diff($today, $last_time));
        $user->save();
        return redirect('/rewards');
    });

    Route::get('/settings', function () {
        return Inertia::render('Settings');
    });

    Route::get('/habit', function () {
        return Inertia::render('Habit');
    });

    Route::post('/habit', function () {
        $habit = Request::get('habit');
        $user = Auth::user();
        $user->habit = $habit;
        $today = new DateTime(date('Y-m-d H:i:s'));
        $user->last_time = $today;
        $user->save();
        return redirect('/profile');
    });

    Route::post('/logout', function () {
        // dd('logging the user out');
    });
});

Route::get('/register', function () {
    return Inertia::render('Register');
});

Route::post('/users',function() {
    $attributes = Request::validate([
        'name' => ['required'],
        'email' => ['required', 'email'],
        'password' => ['required']
    ]);
    $attributes['password'] = bcrypt($attributes['password']);
    \App\Models\User::create($attributes);
    return redirect('/');
});