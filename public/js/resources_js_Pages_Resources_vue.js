(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Resources_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Resources.vue?vue&type=script&lang=js":
/*!**********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Resources.vue?vue&type=script&lang=js ***!
  \**********************************************************************************************************************************************************************************************/
/***/ (() => {

// import { Inertia } from '@inertiajs/inertia';
// export default {
//   props: {
//     users: Object,
//     year: Number,
//     month: Number,
//     day: Number,
//     hour: Number,
//     minute: Number,
//     seconds: Number,
//     errors: Object,
//   },
// setup (){
//   // let form = useForm({
//   //   date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
//   // })
//   let submit = () => {
//     Inertia.post('/users');
//   }
//   return {submit}
// }
// }

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Resources.vue?vue&type=template&id=6e97444c":
/*!**************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Resources.vue?vue&type=template&id=6e97444c ***!
  \**************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm-bundler.js");


var _hoisted_1 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("title", null, "Gameanueva - Resources", -1
/* HOISTED */
);

var _hoisted_2 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("meta", {
  type: "description",
  content: "Resources Description",
  "head-key": "description"
}, null, -1
/* HOISTED */
);

var _hoisted_3 = /*#__PURE__*/(0,vue__WEBPACK_IMPORTED_MODULE_0__.createStaticVNode)("<h1 class=\"text-3xl\">Resources</h1><h2 class=\"text-2xl\">Meetings</h2><p class=\"mb-2\"><u>Sunday</u><br><strong>Solutions</strong><br> Time: 8:30 a.m.<br> Location: <a class=\"break-all\" href=\"tel:1-425-436-6364\" target=\"_blank\">(425) 436-6364</a><br> Passcode: 344330 </p><p class=\"mb-2\"><u>Monday</u><br><strong>Upper Valley SAA</strong><br> Time: 8:00 p.m.<br> Location: <a class=\"break-all\" href=\"https://us04web.zoom.us/j/8345266277?pwd=VHE2QlRkS1BuVURyRzhQcGpLWERtdz09\" target=\"_blank\">https://us04web.zoom.us/j/8345266277?pwd=VHE2QlRkS1BuVURyRzhQcGpLWERtdz09</a><br>Meeting ID: 834 526 6277<br> Password: powerless </p><p class=\"mb-2\"><u>Tuesday</u><br><strong>Mandeville Tuesdayers</strong><br> Time: 6:30 p.m.<br> Location: <a href=\"https://goo.gl/maps/bvd8B9u3oKkt9VR68\" target=\"_blank\">St Michael&#39;s Episcopal Church</a></p><p class=\"mb-2\"><u>Wednesday</u><br><strong>Wednesday Morning Meeting</strong><br> Time: 7:00 a.m.<br> Location: <a class=\"break-all\" href=\"https://zoom.us/j/7572945038\" target=\"_blank\">https://zoom.us/j/7572945038</a><br> Meeting ID: 757 294 5038<br> Password: 1357 </p><p class=\"mb-2\"><u>Thursday</u><br><strong>Outer Circle - Green Book Zoom</strong><br> Time: 7:00 p.m.<br> Location: <a class=\"break-all\" href=\"https://us02web.zoom.us/j/669392515\" target=\"_blank\">https://us02web.zoom.us/j/669392515</a><br> Meeting ID: 669 392 515 <br>Password: circle844 </p><p class=\"mb-2\"><u>Friday</u><br><strong>Friday Morning Meeting</strong> Time: 7:00 a.m.<br> Location: <a class=\"break-all\" href=\"https://zoom.us/j/7572945038\" target=\"_blank\">https://zoom.us/j/7572945038</a><br> Meeting ID: 757 294 5038<br> Password: 1357 </p><p><u>Saturday</u><br><strong>Men in Recovery</strong><br> Time: 7:00 a.m.<br> Location: <a class=\"break-all\" href=\"https://us02web.zoom.us/j/7659493039?pwd=Rkc0NUo2bWtoVFlTWGFmTXAzMHJVUT09\" target=\"_blank\">https://us02web.zoom.us/j/7659493039?pwd=Rkc0NUo2bWtoVFlTWGFmTXAzMHJVUT09</a><br> Zoom ID: 765 949 3039 <br>Passcode: kBP5zH </p><hr><h2 class=\"text-2xl\">My Original Resources</h2><p class=\"mt-2\"><u>My Plan</u><br><a class=\"break-all\" href=\"https://docs.google.com/document/d/1zwh4fmVesrO-r8W8DaU5iG1UN4oVSAgXQ26vXGBmIfM/edit?usp=sharing\" target=\"_blank\">https://docs.google.com/document/d/1zwh4fmVesrO-r8W8DaU5iG1UN4oVSAgXQ26vXGBmIfM/edit?usp=sharing</a></p><p class=\"mt-2\"><u>My Affirmations</u><br> I love myself.<br> I deserve love.<br> I have done bad things, but I am not bad.<br> I am smart.<br> I am caring.<br> I am loving.<br> I mean something to people.<br> I deserve to be here.<br> I am confident.<br> I am a man.<br> I deserve to take care of myself. </p><p class=\"mt-2\"><u>My Letter</u><br><a class=\"break-all\" href=\"https://docs.google.com/document/d/1J_1j8v1QJ1NS1Z4zsqVoNVFepNn36jWr/edit?usp=sharing&amp;ouid=116041855362007142003&amp;rtpof=true&amp;sd=true\" target=\"_blank\">https://docs.google.com/document/d/1J_1j8v1QJ1NS1Z4zsqVoNVFepNn36jWr/edit?usp=sharing&amp;ouid=116041855362007142003&amp;rtpof=true&amp;sd=true</a></p><hr><h2 class=\"text-2xl\">Online Resources for Newcomers</h2><p class=\"mt-2\"><u>Beginner’s Packet</u><br><a class=\"break-all\" href=\"https://saa-recovery.org/literature/getting-started-in-sex-addicts-anonymous-a-beginners-packet-for-recovering-sex-addicts/\" target=\"_blank\">https://saa-recovery.org/literature/getting-started-in-sex-addicts-anonymous-a-beginners-packet-for-recovering-sex-addicts/</a></p><p class=\"mt-2\"><u>Tools of Recovery</u><br><a class=\"break-all\" href=\"https://saa-recovery.org/literature/tools-of-recovery-a-practical-guide-for-new-members-of-saa/\" target=\"_blank\">https://saa-recovery.org/literature/tools-of-recovery-a-practical-guide-for-new-members-of-saa/</a></p><p class=\"mt-2\"><u>Special Welcome to the Woman Newcomer</u><br><a class=\"break-all\" href=\"https://saa-recovery.org/literature/a-special-welcome-to-the-woman-newcomer-from-other-women-members-of-saa/\" target=\"_blank\">https://saa-recovery.org/literature/a-special-welcome-to-the-woman-newcomer-from-other-women-members-of-saa/</a></p><p class=\"mt-2\"><u>Getting a Sponsor</u><br><a class=\"break-all\" href=\"https://saa-recovery.org/literature/getting-a-sponsor/\" target=\"_blank\">https://saa-recovery.org/literature/getting-a-sponsor/</a></p><p class=\"mt-2\"><u>Three Circles Pamphlet</u><br><a class=\"break-all\" href=\"https://saa-recovery.org/literature/three-circles-defining-sexual-sobriety-in-saa/\" target=\"_blank\">https://saa-recovery.org/literature/three-circles-defining-sexual-sobriety-in-saa/</a></p><p class=\"mt-2\"><u>More meeting options:</u></p><a class=\"break-all\" href=\"http://smeetingschicago.com/\" target=\"_blank\">http://smeetingschicago.com/</a><br><a class=\"break-all\" href=\"https://www.scisaa.org/meetings.php\" target=\"_blank\">https://www.scisaa.org/meetings.php</a><br><a class=\"break-all\" href=\"https://www.bayareasaa.org/meetings.php\" target=\"_blank\">https://www.bayareasaa.org/meetings.php</a><br><a class=\"break-all\" href=\"https://www.saa-phoenix.org/meetings/\" target=\"_blank\">https://www.saa-phoenix.org/meetings/</a><br><a class=\"break-all\" href=\"https://portlandsaa.org/meetingsbyday/\" target=\"_blank\">https://portlandsaa.org/meetingsbyday/</a><br><a class=\"break-all\" href=\"https://houstonsaa.org/meeting-schedule/\" target=\"_blank\">https://houstonsaa.org/meeting-schedule/</a><br><a class=\"break-all\" href=\"http://saatalk.info/us/\" target=\"_blank\">http://saatalk.info/us/</a><br><a class=\"break-all\" href=\"http://www.ajourneytohopesaa.org/\" target=\"_blank\">http://www.ajourneytohopesaa.org/</a>", 37);

function render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_Head = (0,vue__WEBPACK_IMPORTED_MODULE_0__.resolveComponent)("Head");

  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)(vue__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, [(0,vue__WEBPACK_IMPORTED_MODULE_0__.createVNode)(_component_Head, null, {
    "default": (0,vue__WEBPACK_IMPORTED_MODULE_0__.withCtx)(function () {
      return [_hoisted_1, _hoisted_2];
    }),
    _: 1
    /* STABLE */

  }), _hoisted_3], 64
  /* STABLE_FRAGMENT */
  );
}

/***/ }),

/***/ "./resources/js/Pages/Resources.vue":
/*!******************************************!*\
  !*** ./resources/js/Pages/Resources.vue ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Resources_vue_vue_type_template_id_6e97444c__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Resources.vue?vue&type=template&id=6e97444c */ "./resources/js/Pages/Resources.vue?vue&type=template&id=6e97444c");
/* harmony import */ var _Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Resources.vue?vue&type=script&lang=js */ "./resources/js/Pages/Resources.vue?vue&type=script&lang=js");
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _Users_marshall_code_gameanueva_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;
const __exports__ = /*#__PURE__*/(0,_Users_marshall_code_gameanueva_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_2__["default"])(_Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_Resources_vue_vue_type_template_id_6e97444c__WEBPACK_IMPORTED_MODULE_0__.render],['__file',"resources/js/Pages/Resources.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (__exports__);

/***/ }),

/***/ "./resources/js/Pages/Resources.vue?vue&type=script&lang=js":
/*!******************************************************************!*\
  !*** ./resources/js/Pages/Resources.vue?vue&type=script&lang=js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* reexport default from dynamic */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0___default.a)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./Resources.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Resources.vue?vue&type=script&lang=js");
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Resources_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
 

/***/ }),

/***/ "./resources/js/Pages/Resources.vue?vue&type=template&id=6e97444c":
/*!************************************************************************!*\
  !*** ./resources/js/Pages/Resources.vue?vue&type=template&id=6e97444c ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Resources_vue_vue_type_template_id_6e97444c__WEBPACK_IMPORTED_MODULE_0__.render)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Resources_vue_vue_type_template_id_6e97444c__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./Resources.vue?vue&type=template&id=6e97444c */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/js/Pages/Resources.vue?vue&type=template&id=6e97444c");


/***/ })

}]);